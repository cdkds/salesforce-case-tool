// ==UserScript==
// @name Content Salesforce Tool REBOOT Working
// @namespace  www.cobaltgroup.com
// @include *://*/*
// @version REBOOT
// @author       Joe Childress
// @grant        none
// @noframes
// ==/UserScript==
// Document ready helper

(function () {


    function getScript(source, callback) {
        let script = document.createElement('script');
        script.async = 1;
        script.onload = script.onreadystatechange = function (_, isAbort) {
            if (isAbort || !script.readyState || /loaded|complete/.test(script.readyState)) {
                script.onload = script.onreadystatechange = null;
                script = undefined;
                if (!isAbort) {
                    if (callback) callback();
                }
            }
        };
        script.src = source;
        document.querySelector("head").appendChild(script);
    }

    //getScript("URL HERE");

    //})(); //END OF TAMPERMONKEY

    function whenDocumentIsReady(fn) {
        if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading") {
            fn();
        }
        else {
            document.addEventListener('DOMContentLoaded', fn);
        }
    }

    whenDocumentIsReady(function () {

      const pathname = window.location.href;

      //CHECK FOR MAIN SF CASE PAGE
      if (pathname.search('cdk.my.salesforce.com/500?')> -1 && pathname.search('500/o') === -1 && pathname.search('fcf=') === -1 && pathname.search('/s') === -1 && pathname.search('/s') === -1 && pathname.search('lkid=') === -1 ){
        getScript('https://code.jquery.com/jquery-3.4.1.min.js', () => {
          getScript('https://media-dmg.assets-cdk.com/teams/repository/export/652/a5dd0de6d1005810f0050568ba825/652a5dd0de6d1005810f0050568ba825.js', initMainTool);
        });
      }
    }); //END OF WHENDOC CALL

    //MAIN SF CASE
    function initMainTool() {

        //ADD GOOGLE FONT
        $('head').append('<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">');
        $('head').append('<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">');

        //FONT AWESOME
        $('head').append('<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">');

        //VARIABLES =======================
        const body = $('body:first').css('position', 'relative');
        const justThePath = window.location.pathname.replace('/', '');
        const webId = $('#CF00N40000002aUB4_ileinner a').text();
        const caseNumber = $.trim( $('.pageDescription').text());
        const qrButton = $("[value*='New Quality Review']");
        const caseID = window.location.href.split("/")[3];
        const closeUrl = 'https://cdk.my.salesforce.com/' + caseID + '/s?retURL=%2F' + caseID;
        const commentThisCase = 'https://cdk.my.salesforce.com/00a/e?parent_id=' + justThePath + '&retURL=%2F' + justThePath;
        const sla = $('#00N330000038Bl4_ileinner').text();

        //$('#00N40000002aU9c_ileinner')[0].innerText ='Proof';

        const prfOrPub = ($('#00N40000002aU9c_ileinner')[0].innerText === 'Proof') ? 'Proof' : 'Publish' || 'Publish' ;
        const prfOrPubIcon = (prfOrPub === 'Proof') ? '<i id ="proof" class="far fa-eye"></i>' : '<i id ="publish" class="far fa-check-circle"></i>';


        //TOOL BOX
        const uiBox = $('<div>', {id: 'uiBox'});
        const uiSubBoxLft = $('<div>', {id: 'uiSubBoxLft'});
        const uiSubBoxRt = $('<div>', {id: 'uiSubBoxRt'});
        const noticeBox = $('<div>', {id: 'noticeBox'});

        //COLORS
        const pubClrLt =  '#62CA67';//MIDGREEN//'#d8f3da';//MIDGREEN//'#EAF9EC'; //LIGHT GREEN
        const proofClrLt = '#fbb113'; //'#EA7A44'; //ORANGE   '#FDF3E7'; //LIGHT ORANGE
        const toolBgColor = (prfOrPub === 'Proof') ? proofClrLt : pubClrLt;  //'#EAF9EC'; //LIGHT GREEN   //'#12B886'; //GREEN //'#F8F9FA'; //LIGHT GRAY
        const textClr = 'white';//'#323232'; //DARK GRAY //'white';
        const noticeClr = '#8de88d';//lightGreen //'#62CA67';//MIDGREEN  //#12B886'; //GREEN

        //ADD TOOL TO PAGE
        body.append(uiBox);
        $("#bodyTable").append(noticeBox);

        //TOOL NODES  =======================

        //LEFT SIDE ************
        //CASE STATUS
        const caseStatus = $('<div>', {id: 'caseStatus', class: 'uiBox-node', title: 'Completion Method'});
        caseStatus.append( prfOrPubIcon +'</i><span class="ui-main">  ' + prfOrPub +'</span>');

        //WEBID
        const copyWebid = $('<div>', {id: 'copyWebid', class: 'uiBox-node uiBox-hvr', title: 'Copy WebId'});
        copyWebid.data('type', 'WebId');
        copyWebid.data('value', webId);
        copyWebid.append('<span class="ui-main-title">webid: </span><span class="ui-main">' + webId +'</span>');

        //CASE NUMBER
        const copyCaseNum = $('<div>', {id: 'caseNumber', class: 'uiBox-node uiBox-hvr', title: 'Copy case number'});
        copyCaseNum.data('type', 'Case Number');
        copyCaseNum.data('value', caseNumber);
        copyCaseNum.append('<span class="ui-main-title">case no: </span><span class="ui-main">' + caseNumber +'</span>');

        //COMMENT
        const commentCase = $('<div>', {id: 'commentCase', class: 'uiBox-node uiBox-hvr', title: 'Add a case comment'});
        commentCase.append('<span class="ui-main">Comment</span>');

        //QA
        const qaLnk = $('<div>', {id: 'qaLnk', class: 'uiBox-node uiBox-hvr', title: 'QA your case'});
        qaLnk.append('<span class="ui-main">QA</span>');

        //CLOSE
        const closeCase = $('<div>', {id: 'closeCase', class: 'uiBox-node uiBox-hvr', title: 'Close case'});
        closeCase.append('<span class="ui-main">Close</span>');

        //NOTICE TEXT
        noticeBox.append('<span class="ui-noticeText"> Copied!</span>');


        //RIGHT SIDE ************
        //FOLDER
        const siteFolder = $('<div>', {id: 'siteFolder', class: 'uiBox-node uiBox-hvr', title: 'Folder Path'});
        siteFolder.append('<span class="ui-main">Folder</i></span>');
        siteFolder.data('type', 'Folder Path');

        //EDIT
        const editSite = $('<div>', {id: 'editSite', class: 'uiBox-node uiBox-hvr', title: 'Edit in WSM'});
        editSite.append('<span class="ui-main">Edit</span>');

        //WIP
        const openWip = $('<div>', {id: 'openWip', class: 'uiBox-node uiBox-hvr', title: 'View WIP site'});
        openWip.append('<span class="ui-main">WIP</span>');

        //LIVE
        const openLive = $('<div>', {id: 'openLive', class: 'uiBox-node uiBox-hvr', title: 'View live site'});
        openLive.append('<span class="ui-main">Live</span>');

        uiSubBoxLft.append(caseStatus);
        uiSubBoxLft.append(copyCaseNum);
        uiSubBoxLft.append(copyWebid);
        uiBox.append(uiSubBoxLft);
        uiSubBoxRt.append(openLive);
        uiSubBoxRt.append(openWip);
        uiSubBoxRt.append(siteFolder);
        uiSubBoxRt.append(qaLnk);


        uiSubBoxRt.append(closeCase);
        uiSubBoxRt.append(commentCase);
        uiSubBoxRt.append(editSite);
        uiBox.append(uiSubBoxRt);

        //HIDE SF CASE NUMBER
        $('.bPageTitle .content').css('visibility','hidden');

        const uiBoxNodes = $('.uiBox-node');
        const uiLinkboxes = $('.uiBox-hvr');


        //STICKY BOX
        const elementPosition = uiBox.offset();

        $(window).scroll(function(){
            if($(window).scrollTop() >= 100){
                uiBox.css('position','fixed').css('top','0');
                noticeBox.css('position','fixed');
            }
            if($(window).scrollTop() < 100){
                uiBox.css('position','absolute').css('top','100px');;
                noticeBox.css('position','absolute').css('top','0');
            }
        });

        $('.uiBox-node').css({
        'font-size': '15px',
        });

        //ELEMENT STYLING ==========================
        uiBox.css({
            'position': 'absolute',
            'z-index': '1100000',
            'background-color': toolBgColor,
            'color': textClr,
            'font-family': '"RobotoBold", sans-serif',
            'background-size': '75%',
            'height': '40px',
            'width': '100%',
            'font-weight': 'bold !important',
            'top': '100px',
            'box-shadow': '0 0 10px rgba(0, 0, 0, 0.5)',
            'box-sizing': 'border-box'
        });

        noticeBox.css({
            'position': 'absolute',
            'z-index': '100000',
            'background-color': noticeClr,
            'color': 'white',
            'text-align': 'center',
            'height': '30px',
            'width': '150px',
            'font-weight': 'bold',
            'top':'0px',
            'margin': '0 45%',
            'padding-top': '8px',
            'border-radius' : '10px',
            'box-sizing': 'border-box',
            'transition': 'all 0.5s ease',
            'overflow': 'hidden',
        });

        uiSubBoxLft.css({
            'float': 'left',
            'height': '100%',
            'box-sizing': 'border-box',
            'margin-left': '40px'
        });

        uiSubBoxLft.children().css({
            'float': 'left'
        });

        uiSubBoxRt.css({
            'float': 'right',
            'height': '100%',
            'box-sizing': 'border-box',
            'margin-right': '40px',
        });

        uiSubBoxRt.children().css({
            'float': 'right'
        });

        uiBoxNodes.css({
            'padding': '12px 16px 0px',
            'text-align': 'center',
            'line-height': '',
            'height': '28px',
            'transition': 'background .05s ease-out',
        });

        $('.ui-main-title').css({'font-size':'9pt'});


        $('.uiBox-hvr').css({'cursor': 'pointer'});

        //URL LOGIC ==========================
        //EDIT URL ***********
        const firstPart = "http://websites.connectcdk.com/wsm/index.do?webId=";
        const secondPartUS = "&locale=en_US";
        const secondPartCAN = "&locale=en_CA";
        const secondPartFRA = "&locale=fr_CA";
        const secondPartHOL = "&locale=en_AU";
        const secondPartHOLNZ = "&locale=en_NZ";
        let wsmUrl = '';

        if (webId.search("gmcl") != -1) {
            if (webId.search("-fr") != -1) {
                wsmUrl = firstPart + webId + secondPartFRA;
            } else {
                wsmUrl = firstPart + webId + secondPartCAN;
            }
        } else if (webId.search("holden") != -1) {
            if (webId.search("holdennz") != -1) {
                wsmUrl = firstPart + webId + secondPartHOLNZ;
            } else {
                wsmUrl = firstPart + webId + secondPartHOL;
            }
        } else {
            wsmUrl = firstPart + webId + secondPartUS;
        }

        //LIVE SITE URL
        /* LIVE AND FOLDER URLS */
        const baseFolderPath = "\\\\las-mgmt1.lasisi01a.las.san.dsghost.net\\Associate\\sea\\CS\\graphics\\manufacturers\\";
        const nitraUrl = "http://nitra.";
        const nitraWip = nitraUrl + 'wip.';
        const proofURLLast = "/?reload=true";
        const webIDArray = webId.split("-");
        const firstString = webIDArray[0];
        const restOfTheWebID = webId.substr(webId.indexOf('-') + 1);
        let liveUrl = '';
        let folderPath = baseFolderPath + firstString + "\\" + restOfTheWebID.charAt(0) + "\\" + restOfTheWebID;
        let oemUrl = '';
        let proofUrl = '';

        //ADD FOLDER PATH
        siteFolder.data('value', folderPath);

        switch (firstString) {
            case 'gmps':
                oemUrl = "gmpsdealer.com/";
                break;

            case 'gmcl':
                oemUrl = "gmcldealer.com/";
                break;

            case 'vw':
                oemUrl = "vwcdkdealer.com/";
                break;

            case 'hyun':
                oemUrl = "hyundaistores.com/";
                break;

            case 'mazda':
                oemUrl = "mazdadealer.com/";
                break;

            case 'lex':
                oemUrl = "lexusdealer.com/";
                folderPath = baseFolderPath + "lexus\\" + restOfTheWebID.charAt(0) + "\\" + restOfTheWebID;
                break;

            case 'k1ia':
                oemUrl = "k1iadealer.com/";
                break;

            case 'b2mw':
                oemUrl = "b2mwdealer.com/";
                break;

            case 'mini':
                oemUrl = "mini-dealer.com/";
                break;

            case 'motp':
                oemUrl = "motorplace.com/";
                folderPath = baseFolderPath + "motorplace\\" + restOfTheWebID.charAt(0) + "\\" + restOfTheWebID;
                break;
            case 'hond':
                oemUrl = "hondadealer.com/";
                folderPath = baseFolderPath + "honda\\" + restOfTheWebID.charAt(0) + "\\" + restOfTheWebID;
                break;
            case 'holden':
                oemUrl = "gmholdendealer.com.au/";
                break;
            case 'holdennz':
                oemUrl =  "gmholdendealer.co.nz/";
                break;
            case 'nissan':
                oemUrl = "nissandealer.com/";
                break;

            case 'toyd':
                oemUrl = "toyotadealer.com/";
                folderPath = baseFolderPath + "toyota\\" + restOfTheWebID.charAt(0) + "\\" + restOfTheWebID;
                break;

            case 'infiniti':
                oemUrl = "infinitidealer.com/";
                break;

            case 'mitsu':
                oemUrl = "mitsudealer.com/";
                break;

            case 'ford':
                oemUrl = "f1rd.com/";
                break;

            case 'cdjr':
                oemUrl = "cdjrdealer.com/";
                break;

            default:
        }

        liveUrl = nitraUrl + oemUrl + restOfTheWebID + proofURLLast;
        proofUrl = nitraWip + oemUrl + restOfTheWebID + proofURLLast;


        // UP ARROW BUTTON =======================
        var toolUpArrow = $('<a id="toolUpArrow" title="Back to top" href="#">&#10148;</a>');

        toolUpArrow.css({
            'width': '30px',
            'line-height': '30px',
            'overflow': 'hidden',
            'z-index': '999',
            'display': 'none',
            'cursor': 'pointer',
            'transform': 'rotate(270deg)',
            'position': 'fixed',
            'bottom': '50px',
            'right': '40px',
            'background-color': '#DDD',
            'color': '#555',
            'text-align': 'center',
            'font-size': '25px',
            'text-decoration': 'none'
        });

        toolUpArrow.hover(function(){this.style.backgroundColor= "#82c600";this.style.color= "white";}, function(){this.style.backgroundColor= "#DDD";this.style.color= "#555";});

        $('body').append(toolUpArrow);


        /*Scroll to top when arrow up clicked BEGIN*/
        $(window).scroll(function() {
            var height = $(window).scrollTop();
            if (height > 100) {
                $('#toolUpArrow').fadeIn();
            } else {
                $('#toolUpArrow').fadeOut();
            }
        });
        $(document).ready(function() {
            $("#toolUpArrow").click(function(event) {
                event.preventDefault();
                $("html, body").animate({ scrollTop: 0 }, "fast");
                return false;
            });

        });
        /*Scroll to top when arrow up clicked END*/

        //END UP ARROW =======================


        //EVENT HANDLERS ==========================

        qaLnk.click(function () {
            qrButton.click();
        });

        commentCase.click(() => window.location =commentThisCase);
        copyWebid.click(copyToClipboard);
        copyCaseNum.click(copyToClipboard);
        siteFolder.click(copyToClipboard);
        closeCase.click(() => window.location =closeUrl);
        editSite.click(() => window.open(wsmUrl, '_blank'));
        openLive.click(() => window.open(liveUrl, '_blank'));
        openWip.click(() => window.open(proofUrl, '_blank'));

        function copyToClipboard(e) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(e.currentTarget).data('value')).select();
            document.execCommand("copy");
            $temp.remove();
            showNotice($(this));
        }

        function showNotice(el) {

            //FIRE NOTIFY ON CLICKED ELEMENT
            el.notify(el.data().type + ' Copied',{
                autoHide: true,
                autoHideDelay: 550,
                className: 'success',
                showDuration: 200,
                arrowShow: true
            });

            //HACK TO GET NOTIFY CHECK BACKGROUND PROPER SIZE
            $('.notifyjs-bootstrap-success').css({'background-position': '2.5px 4.5px'});
        }

    } //initMainTool

})(); //END OF TAMPERMONKEY
